package swallow_log

import (
	"bytes"
	"context"
	"os"
	"strings"
	"testing"
)

func initLogger() {
	ctx := context.Background()
	Init(ctx, "info", "json", "data", true)
}

func TestInit(t *testing.T) {
	initLogger()

	if Logger.Logger.GetLevel().String() != "info" {
		t.Errorf("Logger.Logger.GetLevel().String()(%v) != info", Logger.Logger.GetLevel().String())
	}

	var buf bytes.Buffer
	Logger.Logger.SetOutput(&buf)
	defer func() {
		Logger.Logger.SetOutput(os.Stdout)
	}()

	Logger.Info("test logger")

	l := buf.String()

	if !strings.Contains(l, "info") {
		t.Errorf("log doesn't contain %v", "info")
	}

	if !strings.Contains(l, "test logger") {
		t.Errorf("log doesn't contain %v", "test logger")
	}

	if !strings.Contains(l, "data") {
		t.Errorf("log doesn't contain %v", "data")
	}

	if !strings.Contains(l, "file") {
		t.Errorf("log doesn't contain %v", "file")
	}
}
