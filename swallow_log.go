package swallow_log

import (
	"context"
	log "github.com/sirupsen/logrus"
	"os"
)

var Logger *log.Entry

func Init(ctx context.Context, level string, formatter string, dataKey string, reportCallerInfos bool) {
	if Logger == nil {
		var l log.Logger

		l.SetOutput(os.Stdout)

		Logger = l.WithContext(ctx)

		switch level {
		case "fatal":
			l.SetLevel(log.FatalLevel)
			break
		case "panic":
			l.SetLevel(log.PanicLevel)
			break
		case "warn":
			l.SetLevel(log.WarnLevel)
			break
		case "info":
			l.SetLevel(log.InfoLevel)
			break
		case "debug":
			l.SetLevel(log.DebugLevel)
			break
		}

		switch formatter {
		case "json":
			l.SetFormatter(&log.JSONFormatter{
				DataKey: dataKey,
			})
			break

		default:
			l.SetFormatter(&log.TextFormatter{
				DisableColors: false,
				FullTimestamp: true,
			})
		}

		Logger.Logger.ReportCaller = reportCallerInfos
	}
}
